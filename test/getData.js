const superagent = require("superagent");//模拟
const fs = require('fs');

let url = {
    url_index_footer:{
        // url: "http://api.cloudaward.wang/resources/books/201/chapters/1/content",
        req_header:{
            "Accept": "application/json, text/plain, */*",
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsInN1YiI6IjEiLCJpYXQiOjE1MzM2MDk2NDMsImV4cCI6MTUzNDIxNDQ0MywiZGF0YSI6eyJyb2xlIjoiYWRtaW5pc3RyYXRvciJ9fQ.x2ptCP_SlWzyXo2oBm1rWLFtj-ksY9WZxqC8gpy2ZsM",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"
        }
    },
};
let bid = 199, cno = 36;
function getIndexFooter({ no }){
    return new Promise((resolve)=>{
        superagent.get(`http://api.cloudaward.wang/resources/books/${bid}/chapters/${no}/content`).set(url.url_index_footer.req_header).end(function(err,res){
            if( err ){
                console.log(err);
            }else{
                resolve(res.text);
            }
        })
    })
}

async function getData() { 
    let allData = [];
    for (let i = 1; i <= cno; i++) {
        let data = await getIndexFooter({ no: i });
        data = JSON.parse(data);
        allData.push(data.item);
    }
    let txt = "";
    allData.forEach(item => { 
        txt += `${item.title} \n ${item.content}`
        console.log(item.title);
    })
    fs.writeFile(`./text/${bid}.txt`, txt,  function(err) {
        if (err) {
            return console.error(err);
        }
        console.log('done');
    });
}

getData();

