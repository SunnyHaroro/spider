const superagent = require("superagent");//模拟
const cheerio = require("cheerio");
const fs = require('fs');

let url = {
    url_index_footer:{
        url: "http://mbk.yanjianggongchang.com/wechat/area/artisanFloorHotList",
        req_header:{
            "Accept": "application/json, text/plain, */*",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "zh-CN,zh;q=0.9",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Content-Type": "application/json;charset=UTF-8",
            "Host": "mbk.yanjianggongchang.com",
            "Origin": "http://mbk.yanjianggongchang.com",
            "Pragma": "no-cache",
            "Referer": "http://mbk.yanjianggongchang.com/dist/index.html",
            "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1"
        }
    },
};

function getIndexFooter(obj){
    return new Promise((resolve)=>{
        superagent.post(url.url_index_footer.url).set(url.url_index_footer.req_header).send(obj).end(function(err,res){
            if( err ){
                console.log(err);
            }else{
                resolve(res.text);
            }
        })
    })
}

async function getData(obj) {
    let data = await getIndexFooter(obj);
    return data;
}

getData({currentPage:1}).then((ret) => { 
    console.log(ret);
})